## Homework 
*[#homework]() [#clouds]()*

1. Create your own AWS Free Tier account
2. Create EC2 VM manually (clicking in AWS console)
3. Connect to VM via ssh console, install Docker, Gitlab Runner and register runner in Gitlab with your name as tag

Send screenshot of registered runner to the chat with tag
*[#homework]() [#clouds]()*

Don't forget to stop and kill created EC2 instance. 
